//
//  DepartmentCell.swift
//  Sample Messenger
//
//  Created by Masoud Moharrami on 8/22/17.
//  Copyright © 2017 Masoud Moharrami. All rights reserved.
//

import UIKit

class DepartmentCell: UICollectionViewCell {
    
    @IBOutlet weak var departmentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

//
//  Chatswift
//  Sample Messenger
//
//  Created by Masoud Moharrami on 8/22/17.
//  Copyright © 2017 Masoud Moharrami. All rights reserved.
//

import UIKit

class ChatCell: UICollectionViewCell {
    
    let textView : UITextView = {
        let textView = UITextView()
        textView.backgroundColor = UIColor.clear
        textView.font = UIFont.systemFont(ofSize: 14)
        return textView
    }()
    
    let textBubbleView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0.95, alpha: 1)
        view.layer.cornerRadius = 15
        view.layer.masksToBounds = true
        return view
    }()
    
    let profileImageView : UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    let bubbleImageView: UIImageView = {
        let imageView = UIImageView()
        //        imageView.image = UIImage(named: "bubble")!.resizingImageWithCapInsets(UIEdgeInsets(22,26,22,26))
        return imageView
    }()
    
    let dateLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        
        return label
    }()
    
    var message: Message? {
        didSet{
            if let date = message?.date {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "h:mm a"
                
                let elapsedTimeInSeconds = Date().timeIntervalSince(date as Date)
                
                let secondsInDays: TimeInterval = 60 * 60 * 24
                
                if elapsedTimeInSeconds > 7 * secondsInDays {
                    dateFormatter.dateFormat = "dd/MM/YY"
                }else if elapsedTimeInSeconds > secondsInDays {
                    dateFormatter.dateFormat = "EEE"
                }
                dateLabel.text = dateFormatter.string(from: date as Date)
            }
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textView.isEditable = false
        
        setupView()
    }
    func setupView(){
        
        addSubview(textBubbleView)
        addSubview(textView)
        addSubview(dateLabel)
    }
    func setDate(message: Message){
        if let date = message.date {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "h:mm a"
            
            let elapsedTimeInSeconds = Date().timeIntervalSince(date as Date)
            
            let secondsInDays: TimeInterval = 60 * 60 * 24
            
            if elapsedTimeInSeconds > 7 * secondsInDays {
                dateFormatter.dateFormat = "dd/MM/YY"
            }else if elapsedTimeInSeconds > secondsInDays {
                dateFormatter.dateFormat = "EEE"
            }
            dateLabel.text = dateFormatter.string(from: date as Date)
        }
    }
    func configureCell(message: Message){
        textView.text = message.text
        setDate(message: message)
    }
}

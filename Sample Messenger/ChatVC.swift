//
//  ChatVC.swift
//  Sample Messenger
//
//  Created by Masoud Moharrami on 8/22/17.
//  Copyright © 2017 Masoud Moharrami. All rights reserved.
//

import UIKit
import CoreData

class ChatVC: UIViewController, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, NSFetchedResultsControllerDelegate{
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet var inputTextField: UITextField!
    @IBOutlet var inputTextFieldView: UIView!
    
    let sectionInset = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
    var person: Person!
    
    
    var bottonConstraint: NSLayoutConstraint?
    var dateConstraint: NSLayoutConstraint?
    
    lazy var fetchResultController: NSFetchedResultsController = { () -> NSFetchedResultsController<NSFetchRequestResult> in
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Message")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
        fetchRequest.predicate = NSPredicate(format: "person.name = %@", self.person!.name!)
        
        let delegate = UIApplication.shared.delegate as? AppDelegate
        let context = delegate?.getContext()
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context!, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        return frc
    }()
    
    var blockOperations = [BlockOperation]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        do{
            try fetchResultController.performFetch()
            
        }catch let err{
            print(err)
        }
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        navigationItem.title = person.name
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Simulate", style: .plain, target: self, action: #selector(simulate))
        
        bottonConstraint = NSLayoutConstraint(item: inputTextFieldView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0)
        view.addConstraint(bottonConstraint!)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: .UIKeyboardWillHide, object: nil)
        
    }
    //MARK: collection view functions
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return (fetchResultController.sections?.count)!
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return fetchResultController.sections![0].numberOfObjects
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChatCell", for: indexPath) as? ChatCell{
            // using fetchResultController to fetch messages
            let message = fetchResultController.object(at: indexPath) as! Message
            cell.configureCell(message: message)
            
            let size = CGSize(width: view.frame.width, height: 1000.0)
            let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
            let estimatedFrame = NSString(string: message.text!).boundingRect(with: size, options: options, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14)], context: nil)
            
            let estimatedDateFrame = NSString(string: getDate(message: message)).boundingRect(with: size, options: options, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 12)], context: nil)
            
//            print(" estimatedFrame width: \(estimatedFrame.width)\n estimatedDateFrame width: \(estimatedDateFrame.width)")
//            print("")
            
            if !message.isSender{
                if estimatedFrame.width < estimatedDateFrame.width {
                    cell.textView.frame = CGRect(x: 8, y: 0, width: estimatedDateFrame.width + 14 , height: estimatedFrame.height + 20)
                    cell.dateLabel.frame = CGRect(x: 12, y: estimatedFrame.height + 10, width: estimatedDateFrame.width + 14, height: 14.5)
                    cell.textBubbleView.frame = CGRect(x: 0, y: 0, width: estimatedDateFrame.width + 14 + 8 , height: estimatedFrame.height + 20 + 14.5)
                }else {
                    cell.textView.frame = CGRect(x: 8, y: 0, width: estimatedFrame.width + 14 , height: estimatedFrame.height + 20)
                    cell.dateLabel.frame = CGRect(x: 12 + estimatedFrame.width - estimatedDateFrame.width, y: estimatedFrame.height + 10, width: estimatedDateFrame.width + 14, height: 14.5)
                    cell.textBubbleView.frame = CGRect(x: 0, y: 0, width: estimatedFrame.width + 14 + 8 , height: estimatedFrame.height + 20 + 14.5)
                }
                cell.textBubbleView.backgroundColor = UIColor(white: 0.95, alpha: 1)
                cell.textView.textColor = UIColor.black
                cell.dateLabel.textColor = UIColor.darkGray
            }else {
                
                
                if estimatedFrame.width < estimatedDateFrame.width {
                    cell.textView.frame = CGRect(x: view.frame.width - estimatedDateFrame.width - 14 - 8 - 2 * sectionInset.left - 4 , y: 0, width: estimatedDateFrame.width + 14 , height: estimatedFrame.height + 20)
                    
                    cell.textBubbleView.frame = CGRect(x: view.frame.width - estimatedDateFrame.width  - 14 - 8 - 2 * sectionInset.left - 10, y: 0, width: estimatedDateFrame.width + 14 + 8 , height: estimatedFrame.height + 20 + 14.5)
                    
                    cell.dateLabel.frame = CGRect(x: view.frame.width - estimatedDateFrame.width - 14 - 8 - 2 * sectionInset.left , y:estimatedFrame.height + 10, width: estimatedDateFrame.width + 14, height: 14.5)
                    
                    
                }else{
                    cell.textView.frame = CGRect(x: view.frame.width - estimatedFrame.width - 14 - 8 - 2 * sectionInset.left - 4 , y: 0, width: estimatedFrame.width + 14 , height: estimatedFrame.height + 20)
                    
                    cell.textBubbleView.frame = CGRect(x: view.frame.width - estimatedFrame.width  - 14 - 8 - 2 * sectionInset.left - 10, y: 0, width: estimatedFrame.width + 14 + 8 , height: estimatedFrame.height + 20 + 14.5)
                    
                    cell.dateLabel.frame = CGRect(x: view.frame.width - estimatedDateFrame.width - 14 - 8 - 2 * sectionInset.left , y:estimatedFrame.height + 10, width: estimatedFrame.width + 14, height: 14.5)
                    
                }
                
                cell.textBubbleView.backgroundColor = UIColor(red: 0/255, green: 137/255, blue: 249/255, alpha: 1)
                cell.textView.textColor = UIColor.white
                cell.dateLabel.textColor = UIColor.green
            }
            return cell
        }
        
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInset
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let message = fetchResultController.object(at: indexPath) as! Message
        
        let size = CGSize(width: 250, height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let estimatedFrame = NSString(string: message.text!).boundingRect(with: size, options: options, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14)], context: nil)
        
        return CGSize(width: view.frame.width - 2 * 5 - sectionInset.left * 2, height: estimatedFrame.height + 20 + 14.5)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        inputTextFieldView.endEditing(true)
        bottonConstraint?.constant = 0
    }
    // MARK: Simulate function
    func simulate(){
        print("Simulated, message has been added!")
        
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let context = delegate.getContext()
        
        let text = "Sumulated text"
        
        _ = MessagesVC.createMessageWithText(text: text, person: person, minutesAgo: 0,isSender: false, context: context)
        
        do{
            try context.save()
            
        }catch let err{
            print(err)
        }
        
    }
    @IBAction func sendBtnClicked(_ sender: Any) {
        print("text has been sended")
        
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let context = delegate.getContext()
        
        let text = inputTextField.text
        
        _ = MessagesVC.createMessageWithText(text: text!, person: person, minutesAgo: 0, isSender: true, context: context)
        
        do{
            try context.save()
            inputTextField.text = nil
            
        }catch let err{
            print(err)
        }
        
    }
    func handleKeyboardNotification(notification: Notification){
        if let userInfo = notification.userInfo {
            
            let keyboardFrame: NSValue = userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            
            let isKeyboardShowing = notification.name == .UIKeyboardWillShow
            bottonConstraint?.constant = isKeyboardShowing ? -keyboardHeight: 0
            
            UIView.animate(withDuration: 0, delay: 0, options: .curveEaseOut, animations: {
                self.view.layoutIfNeeded()
            }, completion: { (completed) in
                
                if isKeyboardShowing{
                    let lastItem = (self.fetchResultController.sections?[0].numberOfObjects)! - 1
                    let indexPath = IndexPath(item: lastItem, section: 0)
                    
                    self.collectionView.scrollToItem(at: indexPath, at: .bottom, animated: true)
                }
                
            })
            
        }
    }
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        if type == .insert{
            blockOperations.append(BlockOperation(block: {
                self.collectionView.insertItems(at: [newIndexPath!])
            }))
        }
    }
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        collectionView?.performBatchUpdates({
            for operation in self.blockOperations{
                operation.start()
            }
        }, completion: {(completed) in
            let lastItem = (self.fetchResultController.sections?[0].numberOfObjects)! - 1
            let indexPath = IndexPath(item: lastItem, section: 0)
            
            self.collectionView.scrollToItem(at: indexPath, at: .bottom, animated: true)
        })
    }
    func getDate(message: Message) -> String {
        if let date = message.date {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "h:mm a"
            
            let elapsedTimeInSeconds = Date().timeIntervalSince(date as Date)
            
            let secondsInDays: TimeInterval = 60 * 60 * 24
            
            if elapsedTimeInSeconds > 7 * secondsInDays {
                dateFormatter.dateFormat = "dd/MM/YY"
            }else if elapsedTimeInSeconds > secondsInDays {
                dateFormatter.dateFormat = "EEE"
            }
            let dateText = dateFormatter.string(from: date as Date)
            //            print(dateText.characters.count)
            return dateText
        }
        return ""
    }
    
}



















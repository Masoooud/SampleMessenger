//
//  Message+CoreDataProperties.swift
//  Sample Messenger
//
//  Created by Masoud Moharrami on 8/28/17.
//  Copyright © 2017 Masoud Moharrami. All rights reserved.
//

import Foundation
import CoreData


extension Message {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Message> {
        return NSFetchRequest<Message>(entityName: "Message")
    }

    @NSManaged public var date: NSDate?
    @NSManaged public var isSender: Bool
    @NSManaged public var text: String?
    @NSManaged public var person: Person?

}

//
//  MessageCell.swift
//  Sample Messenger
//
//  Created by Masoud Moharrami on 8/22/17.
//  Copyright © 2017 Masoud Moharrami. All rights reserved.
//

import UIKit

class MessageCell: UICollectionViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var hasReadImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override var isHighlighted: Bool {
        didSet{
            backgroundColor = isHighlighted ? UIColor.init(red: 0, green: 134/255, blue: 249/255, alpha: 1) : UIColor.white
            
            nameLabel.textColor = isHighlighted ? UIColor.white : UIColor.darkGray
            messageLabel.textColor = isHighlighted ? UIColor.white : UIColor.darkGray
            dateLabel.textColor = isHighlighted ? UIColor.white : UIColor.darkGray
            
        }
    }
    var message: Message? {
        didSet{
            nameLabel.text = message?.person?.name
            messageLabel.text = message?.text
            if let profileImage = message?.person?.profileImage {
                profileImageView.image = UIImage(named: profileImage)
                hasReadImageView.image = UIImage(named: profileImage)
            }
            if let date = message?.date {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "h:mm a"
                
                let elapsedTimeInSeconds = Date().timeIntervalSince(date as Date)
                
                let secondsInDays: TimeInterval = 60 * 60 * 24
                
                if elapsedTimeInSeconds > 7 * secondsInDays {
                    dateFormatter.dateFormat = "dd/MM/YY"
                }else if elapsedTimeInSeconds > secondsInDays {
                    dateFormatter.dateFormat = "EEE"
                }
                dateLabel.text = dateFormatter.string(from: date as Date)
            }
        }
    }
}


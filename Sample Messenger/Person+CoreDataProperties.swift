//
//  Person+CoreDataProperties.swift
//  Sample Messenger
//
//  Created by Masoud Moharrami on 8/29/17.
//  Copyright © 2017 Masoud Moharrami. All rights reserved.
//

import Foundation
import CoreData


extension Person {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Person> {
        return NSFetchRequest<Person>(entityName: "Person")
    }

    @NSManaged public var name: String?
    @NSManaged public var profileImage: String?
    @NSManaged public var messages: NSSet?
    @NSManaged public var lastMessage: Message?

}

// MARK: Generated accessors for messages
extension Person {

    @objc(addMessagesObject:)
    @NSManaged public func addToMessages(_ value: Message)

    @objc(removeMessagesObject:)
    @NSManaged public func removeFromMessages(_ value: Message)

    @objc(addMessages:)
    @NSManaged public func addToMessages(_ values: NSSet)

    @objc(removeMessages:)
    @NSManaged public func removeFromMessages(_ values: NSSet)

}

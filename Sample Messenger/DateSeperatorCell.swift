//
//  dateSeperatorCell.swift
//  Sample Messenger
//
//  Created by Masoud Moharrami on 9/2/17.
//  Copyright © 2017 Masoud Moharrami. All rights reserved.
//

import UIKit

class DateSeperatorCell: UICollectionViewCell {
    
    let dateLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        
        return label
    }()
    
    var message: Message? {
        didSet{
            if let date = message?.date {
                let dateFormatter = DateFormatter()                
                let elapsedTimeInSeconds = Date().timeIntervalSince(date as Date)
                
                let secondsInDays: TimeInterval = 60 * 60 * 24
                
                if elapsedTimeInSeconds > 7 * secondsInDays {
                    dateFormatter.dateFormat = "dd/MM/YY"
                }else if elapsedTimeInSeconds > secondsInDays {
                    dateFormatter.dateFormat = "EEE"
                }
                dateLabel.text = dateFormatter.string(from: date as Date)
            }
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupView()
    }
    func setupView(){
        addSubview(dateLabel)
    }
    func setDate(message: Message){
        if let date = message.date {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "h:mm a"
            
            let elapsedTimeInSeconds = Date().timeIntervalSince(date as Date)
            
            let secondsInDays: TimeInterval = 60 * 60 * 24
            
            if elapsedTimeInSeconds > 7 * secondsInDays {
                dateFormatter.dateFormat = "dd/MM/YY"
            }else if elapsedTimeInSeconds > secondsInDays {
                dateFormatter.dateFormat = "EEE"
            }
            dateLabel.text = dateFormatter.string(from: date as Date)
        }
    }
    func configureCell(message: Message){
        setDate(message: message)
    }
}

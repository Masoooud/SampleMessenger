//
//  MaterialButton.swift
//  Samantel
//
//  Created by Masoud Moharrami on 8/13/17.
//  Copyright © 2017 Masoud Moharrami. All rights reserved.
//

import UIKit

@IBDesignable class MaterialButton: UIButton {
    
    @IBInspectable var background: UIColor? = UIColor.darkGray{
        didSet{
            self.backgroundColor = background
        }
    }
    @IBInspectable var masksToBounds: Bool = false {
        didSet{
            layer.masksToBounds = masksToBounds
        }
    }
    @IBInspectable var cornerRadius: CGFloat = 3.0 {
        didSet{
            layer.cornerRadius = cornerRadius
        }
    }
    @IBInspectable var shadowRadius: CGFloat = 3.0 {
        didSet{
            layer.shadowRadius = shadowRadius
            layer.masksToBounds = false
        }
    }
    @IBInspectable var shadowOpacity: Float = 0.8{
        didSet{
            layer.shadowOpacity = shadowOpacity
            layer.masksToBounds = false
        }
    }
    @IBInspectable var shadowOffset: CGSize = CGSize(){
        didSet{
            layer.shadowOffset = shadowOffset
            layer.masksToBounds = false
        }
    }
    @IBInspectable var shadowColor: CGColor = UIColor.darkGray.cgColor {
        didSet{
            layer.shadowColor = shadowColor
            layer.masksToBounds = false
        }
    }
    
}

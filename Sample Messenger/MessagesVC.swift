//
//  ViewController.swift
//  Sample Messenger
//
//  Created by Masoud Moharrami on 8/21/17.
//  Copyright © 2017 Masoud Moharrami. All rights reserved.
//

import UIKit
import CoreData

class MessagesVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, NSFetchedResultsControllerDelegate{
    
    let sectionInsets = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
    let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    
    var blockOperations = [BlockOperation]()
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    lazy var fetchResultController: NSFetchedResultsController = { () -> NSFetchedResultsController<NSFetchRequestResult> in
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Person")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "lastMessage.date", ascending: false)]
        fetchRequest.predicate = NSPredicate(format: "lastMessage != nil")
        let delegate = UIApplication.shared.delegate as? AppDelegate
        let context = delegate?.getContext()
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context!, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        return frc
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.alwaysBounceVertical = true
        collectionView.contentInset = contentInsets
        
        setupData()
        
        do{
            try fetchResultController.performFetch()
        }catch let err{
            print(err)
        }
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "New", style: .plain, target: self, action: #selector(addNewPerson))
    }
    
    func addNewPerson(){
//        fetching information form database
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.getContext()
        
        let john = NSEntityDescription.insertNewObject(forEntityName: "Person", into: context) as? Person
        john?.name = "John Snow"
        john?.profileImage = "john-snow"
        
        _ = MessagesVC.createMessageWithText(text: "Hello, how are you?", person: john!, minutesAgo: 5, context: context)
        appDelegate.saveContext()
    }
    
    func setupData(){
        
        clearData()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.getContext()
        
        let john = NSEntityDescription.insertNewObject(forEntityName: "Person", into: context) as? Person
        john?.name = "John Snow"
        john?.profileImage = "john-snow"
        
        _ = MessagesVC.createMessageWithText(text: "Hello, how are you?", person: john!, minutesAgo: 5, context: context)
        _ = MessagesVC.createMessageWithText(text: "How is going bro? Everything is ok?", person: john!, minutesAgo: 3, context: context)
        _ = MessagesVC.createMessageWithText(text: "I'd like to see you in tomarrow morning at 8?", person: john!, minutesAgo: 1, context: context)
        _ = MessagesVC.createMessageWithText(text: "Hello, I'm good", person: john!, minutesAgo: 4, isSender: true, context: context)
        _ = MessagesVC.createMessageWithText(text: "Nothing special", person: john!, minutesAgo: 2, isSender: true, context: context)
        _ = MessagesVC.createMessageWithText(text: "Hello, how are you?", person: john!, minutesAgo: 5, context: context)
        _ = MessagesVC.createMessageWithText(text: "How is going bro? Everything is ok?", person: john!, minutesAgo: 3, context: context)
        _ = MessagesVC.createMessageWithText(text: "I'd like to see you in tomarrow morning at 8?", person: john!, minutesAgo: 1, context: context)
        _ = MessagesVC.createMessageWithText(text: "Hello, I'm good", person: john!, minutesAgo: 4, isSender: true, context: context)
        _ = MessagesVC.createMessageWithText(text: "Nothing special", person: john!, minutesAgo: 2, isSender: true, context: context)
        
        
        let steve = NSEntityDescription.insertNewObject(forEntityName: "Person", into: context) as? Person
        steve?.name = "Steve Jobs"
        steve?.profileImage = "steve-jobs"
        
        _ = MessagesVC.createMessageWithText(text: "Everything is allright?", person: steve!, minutesAgo: 60 * 24, context: context)
        
        let masoud = NSEntityDescription.insertNewObject(forEntityName: "Person", into: context) as? Person
        masoud?.name = "Masoud Moharrami"
        masoud?.profileImage = "profile-photo-square"
        
        _ = MessagesVC.createMessageWithText(text: "How is going bro?", person: masoud!, minutesAgo: 8 * 60 * 24, context: context)
        _ = MessagesVC.createMessageWithText(text: "Everything is ok?", person: masoud!, minutesAgo: 8 * 60 * 24, context: context)
        // saveing informations
        appDelegate.saveContext()

    }
    static func createMessageWithText(text: String, person: Person, minutesAgo: Double, isSender: Bool = false, context: NSManagedObjectContext) -> Message{
        let message = NSEntityDescription.insertNewObject(forEntityName: "Message", into: context) as? Message
        message?.person = person
        message?.text = text
        message?.date = NSDate().addingTimeInterval(-minutesAgo * 60)
        message?.isSender = isSender
        
        person.lastMessage = message
        
        return message!
    }
    func clearData(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.getContext()
        do{
            let entityNames = ["Person", "Message"]
            for entityName in entityNames{
                let request =  NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
                let objects = try(context.fetch(request)) as? [NSManagedObject]
                for object in objects!{
                    context.delete(object)
                }
            }
            try (context.save())
        }catch let err{
            print(err)
        }
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return (fetchResultController.sections?.count)!
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return fetchResultController.sections![0].numberOfObjects
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MessageCell", for: indexPath) as? MessageCell{
            
            let person = fetchResultController.object(at: indexPath) as? Person
            cell.message = person?.lastMessage
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        return CGSize(width: collectionView.frame.width - contentInsets.left * 2.0 - sectionInsets.left * 2.0, height: 100)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let chatVC = storyboard?.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
        chatVC.person = fetchResultController.object(at: indexPath) as! Person
        navigationController?.pushViewController(chatVC, animated: true)
    }
    // MARK: data controlling
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        if type == .insert{
            blockOperations.append(BlockOperation(block: {
                self.collectionView.insertItems(at: [newIndexPath!])
            }))
            
        }
    }
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        collectionView?.performBatchUpdates({
            for operation in self.blockOperations{
                operation.start()
            }
        }, completion: {(completed) in
            let lastItem = (self.fetchResultController.sections?[0].numberOfObjects)! - 1
            let indexPath = IndexPath(item: lastItem, section: 0)
            
            self.collectionView.scrollToItem(at: indexPath, at: .bottom, animated: true)
        })
    }
}

















